import app from "./server.js"
import mongodb from "mongodb"
//import ReviewsDAO from "./dao/reviewsDAO.js"

const MongoClient=mongodb.MongoClient
const mongo_username=process.env(MONGO_USERNAME)
const mongo_password=process.env(MOMGO_PASSWORD)
const uri="mongodb+srv://${mongo_username}:${mongo_password}@cluster0.knbgywy.mongodb.net/test"
const port=8000


MongoClient .connect(
    uri,
    {
        maxPoolsize:50,
        wtimeoutMS:2500,
        useNewURLParser:true
    }
)

.catch(err=>{
    console.error(err.stack)
    process.exit(1)
})

.then(async client=> {
    app.listen(port, ()=>{
        console.log('listening on port ${port}')
    })
})
